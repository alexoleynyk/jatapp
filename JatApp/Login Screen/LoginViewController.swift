//
//  ViewController.swift
//  JatApp
//
//  Created by Alex Oleynyk on 05.09.2018.
//  Copyright © 2018 oleynyk.com. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var nameTextField: LoginTextField!
    @IBOutlet weak var emailTextField: LoginTextField!
    @IBOutlet weak var passwordTextField: LoginTextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var signupButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    
    private var loginViewModel: LoginViewModel!
    
    static func storyboardInstance() -> LoginViewController? {
        let storyboard = UIStoryboard(name: String(describing: self), bundle: nil)
        return storyboard.instantiateInitialViewController() as? LoginViewController
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        loginViewModel = LoginViewModel()
        
        setKeyboardNotificationObservers()
        setGestures()
        
        nameTextField.delegate = self
        emailTextField.delegate = self
        passwordTextField.delegate = self
    }
    
    //MARK: - Gesture and notification methods
    
    @objc func swippedDown(_ sender: UITapGestureRecognizer? = nil) {
        view.endEditing(true)
    }
    
    @objc func keyboardShow(notification: NSNotification) {
        
        if let activeFieldIndex = loginViewModel.activeFieldIndex {
            var frame: CGRect = .zero
            switch activeFieldIndex {
            case 0:
                frame = nameTextField.convert(nameTextField.frame, to: self.view)
            case 1:
                frame = emailTextField.convert(emailTextField.frame, to: self.view)
            case 2:
                frame = passwordTextField.convert(passwordTextField.frame, to: self.view)
            default:
                break
            }
            
            var keyboardHeight = CGFloat(210)
            if let userInfo = notification.userInfo,
                let keyboardHeightValue = userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue{
                keyboardHeight = keyboardHeightValue.cgRectValue.height
                if #available(iOS 11.0, *) {
                    keyboardHeight += view.safeAreaInsets.bottom
                }
            }
            
            let offsetHeight = keyboardHeight - (self.view.frame.height - frame.maxY) - 10
            
            UIView.animate(withDuration: 0.5, delay: 0, options: [.curveEaseInOut], animations: { [weak self] in
                if offsetHeight < 0 { return }
                self?.view.frame = CGRect(x: 0, y: -offsetHeight, width: self?.view.frame.width ?? 0, height: self?.view.frame.height ?? 0)
                }, completion: nil)
        }
    }
    
    @objc func keyboardHide() {
        UIView.animate(withDuration: 0.5, delay: 0, options: [.curveEaseInOut], animations: { [weak self] in
            self?.view.frame = CGRect(x: 0, y: 0, width: self?.view.frame.width ?? 0, height: self?.view.frame.height ?? 0)
            }, completion: nil)
    }
    
    //MARK: - IBActions

    @IBAction func switchSigninSignup(_ sender: UIButton) {
        let inSignupMode = loginViewModel.switchSigninSignup()
        if let nameView = stackView.subviews.first {
            UIView.animate(withDuration: 0.4) {
                nameView.layer.opacity = inSignupMode ? 1 : 0
            }
        }
        
        loginButton.setTitle(loginViewModel.loginButtonText, for: .normal)
        signupButton.setTitle(loginViewModel.signupButtonText, for: .normal)
    }
    
    @IBAction func loginButtonTapped(_ sender: UIButton) {
        let inSignupMode = loginViewModel.inSignupMode
        setInterfaceInActiveState(false)
        
        let nameInTextField = nameTextField.text == "" ? nil : nameTextField.text
        let name = inSignupMode ?  nameInTextField : nil
        let email = emailTextField.text ?? "No email"
        let password = passwordTextField.text ?? "1234"
        let user = User(name: name, email: email, password: password, accessToken: nil)
        loginViewModel.user = user
        
        if inSignupMode {
            loginViewModel.signUp(completion: singUpSignInHandler(error:user:))
        } else {
            loginViewModel.signIn(completion: singUpSignInHandler(error:user:))
        }
    }
    
    //MARK: - Private functions
    private func setKeyboardNotificationObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    private func setGestures() {
        let swipeDownGesture = UISwipeGestureRecognizer(target: self, action: #selector(swippedDown(_:)))
        swipeDownGesture.direction = .down
        self.view.addGestureRecognizer(swipeDownGesture)
    }
    
    private func singUpSignInHandler(error: Error?, user: User?) {
        setInterfaceInActiveState(true)
        guard error == nil else {
            ErrorHandler().handleError(error!.localizedDescription)
            return
        }
        
        if let newUser = user {
            showUserScreen(withUser: newUser)
        }
        
    }
    
    
    private func setInterfaceInActiveState(_ isActive: Bool) {
        if isActive {
            activityIndicator.stopAnimating()
            activityIndicator.layer.opacity = 0
            loginButton.setTitle(loginViewModel.loginButtonText, for: .normal)
            view.isUserInteractionEnabled = true
        } else {
            loginButton.setTitle("", for: .normal)
            view.isUserInteractionEnabled = false
            activityIndicator.startAnimating()
            activityIndicator.layer.opacity = 1
        }
    }
    
    private func showUserScreen(withUser user: User) {
        if let userScreenController = UserViewController.storyboardInstance() {
            
            userScreenController.userViewModel = UserViewModel(user: user)
            present(userScreenController, animated: true) { [weak self] in
                self?.nameTextField.text = ""
                self?.emailTextField.text = ""
                self?.passwordTextField.text = ""
                self?.loginViewModel.user = nil
            }
        }
    }
    
}

extension LoginViewController: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        switch textField {
        case nameTextField:
            loginViewModel.activeFieldIndex = 0
        case emailTextField:
            loginViewModel.activeFieldIndex = 1
        case passwordTextField:
            loginViewModel.activeFieldIndex = 2
        default:
            loginViewModel.activeFieldIndex = nil
        }
    
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        switch textField {
        case nameTextField:
            emailTextField.becomeFirstResponder()
        case emailTextField:
            passwordTextField.becomeFirstResponder()
        case passwordTextField:
            view.endEditing(false)
        default:
            break
        }
        
        return true
    }
}

