//
//  LoginViewModel.swift
//  JatApp
//
//  Created by Alex Oleynyk on 05.09.2018.
//  Copyright © 2018 oleynyk.com. All rights reserved.
//

import Foundation


class LoginViewModel {
    
    var user: User?
    var inSignupMode = false
    var activeFieldIndex: Int?
    
    var loginButtonText: String {
        return inSignupMode ? "SIGN UP" : "LOG IN"
    }
    
    var signupButtonText: String {
        return inSignupMode ? "I have an account" : "Don't have an account?"
    }
    
    func switchSigninSignup() -> Bool {
        inSignupMode = !inSignupMode
        return inSignupMode
    }
    
    func signUp(completion: @escaping (Error?, User?) -> ()) {
        if let user = user {
            ApiService().signUp(withUser: user) { (error, newUser) in
                guard error == nil else {
                    completion(error, nil)
                    return
                }
                
                if let newUser = newUser {
                    completion(nil, newUser)
                }
            }
        }
    }
    
    func signIn(completion: @escaping  (Error?, User?)  -> ()) {
        if let user = user {
            ApiService().signIn(withUser: user) { (error, newUser) in
                guard error == nil else {
                    completion(error, nil)
                    return
                }
                
                if let newUser = newUser {
                    completion(nil, newUser)
                }
            }
        }
    }
    
}
