//
//  UserViewModel.swift
//  JatApp
//
//  Created by Alex Oleynyk on 06.09.2018.
//  Copyright © 2018 oleynyk.com. All rights reserved.
//

import Foundation

class UserViewModel {
    
    var user: User
    var charactersArray: [(Character, CharacterData)] = []
    
    init(user: User) {
        self.user = user
    }
    
    var greetingsString: String {
        return "Hi, \(user.name!)"
    }
    
    func getResultsCellViewModel(forIndex index: Int) -> ResultsCellViewModel {
        let (char, charData) = charactersArray[index]
        let viewModel = ResultsCellViewModel(char: char, charachterData: charData)
        return viewModel
    }
    
    func fetchText(completion: @escaping (Error?) -> ()) {
        ApiService().getText(withUser: user) { [weak self] (error, text) in
            guard error == nil else {
                completion(error)
                return
            }
            
            if let text = text {
                let analizer = TextAnalyzer(withText: text)
                let charactersArray = analizer.processText().sortedArray()
                self?.charactersArray = charactersArray
                completion(nil)
            }
        }
    }
    
    func logOut(completion: @escaping (Error?) -> ()) {
        ApiService().logOut(withUser: user) { error in
            guard error == nil else {
                completion(error)
                return
            }
            completion(nil)
        }
    }
}
