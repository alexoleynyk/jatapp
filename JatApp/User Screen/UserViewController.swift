//
//  UserViewController.swift
//  JatApp
//
//  Created by Alex Oleynyk on 05.09.2018.
//  Copyright © 2018 oleynyk.com. All rights reserved.
//

import UIKit

class UserViewController: UIViewController {
    @IBOutlet weak var resultsTableView: UITableView!
    @IBOutlet weak var greetingsLabel: UILabel!
    @IBOutlet weak var reloadButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var userViewModel: UserViewModel!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    static func storyboardInstance() -> UserViewController? {
        let storyboard = UIStoryboard(name: String(describing: self), bundle: nil)
        return storyboard.instantiateInitialViewController() as? UserViewController
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        resultsTableView.delegate = self
        resultsTableView.dataSource = self
        
        greetingsLabel.text = userViewModel.greetingsString
        
        fetchText()
    }
    
    //MARK: - IBActions
    
    @IBAction func reloadPressed(_ sender: UIButton) {
        fetchText()
    }
    
    @IBAction func logoutPressed(_ sender: UIButton) {
        logOut()
    }

    //MARK: - Private functions
    
    private func logOut() {
        userViewModel.logOut { (error) in
            if error != nil  {
            ErrorHandler().handleError(error!.localizedDescription)
            }
            
        }
        dismiss(animated: true, completion: nil)
    }
    
    private func fetchText() {
        setReloadButtonInActiveState(false)
        userViewModel.fetchText { [weak self] (error) in
            self?.setReloadButtonInActiveState(true)
            
            guard error == nil else {
                ErrorHandler().handleError(error!.localizedDescription)
                return
            }
            self?.resultsTableView.reloadData()
        }
    }
    
    private func setReloadButtonInActiveState(_ isActive: Bool) {
        if isActive {
            activityIndicator.stopAnimating()
            activityIndicator.layer.opacity = 0
            reloadButton.layer.opacity = 1
        } else {
            activityIndicator.startAnimating()
            activityIndicator.layer.opacity = 1
            reloadButton.layer.opacity = 0
        }
    }

    
    
}

extension UserViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userViewModel.charactersArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ResultsTableCell", for: indexPath) as! ResultsTableViewCell
        
        let index = indexPath.row
        let viewModel = userViewModel.getResultsCellViewModel(forIndex: index)
        
        cell.configureCell(withViewModel: viewModel)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 62.0
    }

}
