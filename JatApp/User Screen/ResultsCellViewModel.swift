//
//  ResultsCellViewModel.swift
//  JatApp
//
//  Created by Alex Oleynyk on 06.09.2018.
//  Copyright © 2018 oleynyk.com. All rights reserved.
//

import Foundation
import UIKit

struct ResultsCellViewModel {

    private let char: Character
    private let count: Int
    private let frequency: Float
    
    init(char: Character, charachterData: CharacterData) {
        self.char = char
        self.count = charachterData.count
        self.frequency = charachterData.frequency
    }
    
    var characterString: String {
        let displayCharacterString = char == " " ? "space" : "\(char)"
        return "\'\(displayCharacterString)\' character"
    }
    
    var statsString: String {
        return "\(count) times, frequency: \(frequency)"
    }
    
    var frequencyMarkerColor: UIColor {
        switch frequency {
        case 0..<0.025:
            return UIColor.green
        case 0.025..<0.05 :
            return UIColor.orange
        default:
            return UIColor.red
        }
        
    }
    
}
