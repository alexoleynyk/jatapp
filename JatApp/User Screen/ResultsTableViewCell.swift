//
//  ResultTableViewCell.swift
//  JatApp
//
//  Created by Alex Oleynyk on 06.09.2018.
//  Copyright © 2018 oleynyk.com. All rights reserved.
//

import UIKit

class ResultsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var frequencyMarker: UIView!
    @IBOutlet weak var characterLabel: UILabel!
    @IBOutlet weak var statsLabel: UILabel!
    
    func configureCell(withViewModel viewModel: ResultsCellViewModel) {
        cardView.layer.cornerRadius = 10
        frequencyMarker.layer.cornerRadius = 5
        frequencyMarker.backgroundColor = viewModel.frequencyMarkerColor
        characterLabel.text = viewModel.characterString
        statsLabel.text = viewModel.statsString
    }

}
