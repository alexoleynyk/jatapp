//
//  User.swift
//  JatApp
//
//  Created by Alex Oleynyk on 06.09.2018.
//  Copyright © 2018 oleynyk.com. All rights reserved.
//

struct User {
    var name: String?
    let email: String
    let password: String
    var accessToken: String?
}
