//
//  CharacterData.swift
//  JatApp
//
//  Created by Alex Oleynyk on 06.09.2018.
//  Copyright © 2018 oleynyk.com. All rights reserved.
//

struct CharacterData {
    var count: Int
    var frequency: Float
}
