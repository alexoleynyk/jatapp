//
//  UserError.swift
//  JatApp
//
//  Created by Alex Oleynyk on 06.09.2018.
//  Copyright © 2018 oleynyk.com. All rights reserved.
//

import Foundation

enum UserError: LocalizedError {
    case userHasNoName
    case userHasNoAccessToken
    
    public var errorDescription: String? {
        switch self {
        case .userHasNoName:
            return "User must have a name for signing up"
        case .userHasNoAccessToken:
            return "You must provide a accessToken for using API methods"
        }
    }
}
