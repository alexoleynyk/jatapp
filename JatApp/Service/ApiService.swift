//
//  DataProvider.swift
//  JatApp
//
//  Created by Alex Oleynyk on 05.09.2018.
//  Copyright © 2018 oleynyk.com. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class ApiService {
    
    func signIn(withUser user: User, completion: @escaping (_ error: Error?, _ user: User?) -> Void) {
        guard let loginUrl = URL(string: "https://apiecho.cf/api/login/") else { return }
        
        let params = [
            "email": user.email,
            "password": user.password
        ]
        
        let headers: HTTPHeaders = [
            "Accept": "application/json"
        ]

        request(loginUrl, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers)
            .validate()
            .responseJSON { (response) in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    let name = json["data"]["name"].string ?? "%username%"
                    let accessToken = json["data"]["access_token"].string ?? nil
                    let newUser = User(name: name, email: user.email, password: user.password, accessToken: accessToken)
                    completion(nil, newUser)
                case .failure(let error):
                    completion(error, nil)
                }
            }
    }
    
    func getText(withUser user: User, completion: @escaping (_ error: Error?, _ text: String?) -> Void) {
        guard let getTextUrl = URL(string: "https://apiecho.cf/api/get/text/") else { return }
        
        guard let accessToken = user.accessToken else {
            completion(UserError.userHasNoAccessToken, nil)
            return
        }
        
        let params = [
            "Locale": "en_US"
        ]
        
        let headers: HTTPHeaders = [
            "Accept": "application/json",
            "Authorization": "Bearer \(accessToken)"
        ]
        
        request(getTextUrl, method: .get, parameters: params, encoding: JSONEncoding.default, headers: headers)
            .validate()
            .responseJSON { (response) in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    let text = json["data"].string ?? nil
                    completion(nil, text)
                case .failure(let error):
                    completion(error, nil)
                }
            }
    }
    
    func signUp(withUser user: User, completion: @escaping (_ error: Error?, _ user: User?) -> Void) {
        guard let loginUrl = URL(string: "https://apiecho.cf/api/signup/") else { return }
        
        guard let userName = user.name else {
            completion(UserError.userHasNoName, user)
            return
        }
        
        let params = [
            "name": userName,
            "email": user.email,
            "password": user.password
        ]
        
        let headers: HTTPHeaders = [
            "Accept": "application/json"
        ]
        
        request(loginUrl, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers)
            .validate()
            .responseJSON { (response) in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    let name = json["data"]["name"].string ?? "%username%"
                    let accessToken = json["data"]["access_token"].string ?? nil
                    let newUser = User(name: name, email: user.email, password: user.password, accessToken: accessToken)
                    completion(nil, newUser)
                case .failure(let error):
                    completion(error, user)
                }
            }
    }
    
    func logOut(withUser user: User, completion: @escaping (_ error: Error?) -> Void) {
        guard let getTextUrl = URL(string: "https://apiecho.cf/api/logout/") else { return }
        
        guard let accessToken = user.accessToken else {
            completion(UserError.userHasNoAccessToken)
            return
        }
        
        let headers: HTTPHeaders = [
            "Accept": "application/json",
            "Authorization": "Bearer \(accessToken)"
        ]
        
        request(getTextUrl, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: headers)
            .validate()
            .responseJSON { (response) in
                switch response.result {
                case .success:
                    completion(nil)
                case .failure(let error):
                    completion(error)
                }
            }
    }

}
