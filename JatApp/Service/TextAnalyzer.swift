//
//  TextAnalyzer.swift
//  JatApp
//
//  Created by Alex Oleynyk on 05.09.2018.
//  Copyright © 2018 oleynyk.com. All rights reserved.
//

import Foundation

class TextAnalyzer {
    
    private var text: String
    private var count: Int
    private var charStatistic: [Character: CharacterData] = [:]
    
    init(withText text: String) {
        self.text = text
        self.count = text.count
    }
    
    func processText() -> TextAnalyzer {
        let dict = text.reduce([:]) { (dictionary, char) -> [Character: CharacterData] in
            var dict = dictionary
            
            var count = dict[char]?.count ?? 0
            count += 1
            let data = CharacterData(count: count, frequency: 0)
            dict[char] = data
            return dict
        }.mapValues { value -> CharacterData in
            let frecuency = Float(value.count) / Float (self.count)
            return CharacterData(count: value.count, frequency: frecuency)
        }
        charStatistic = dict
        return self
    }
    
    func sortedArray() -> [(key: Character, value: CharacterData)] {
        return charStatistic.sorted(by: { $0.value.count > $1.value.count })
    }
}
